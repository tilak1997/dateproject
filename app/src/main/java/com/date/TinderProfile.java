package com.date;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class TinderProfile extends AppCompatActivity {

    ImageView profile_pic;
    TextView ageName;
    TextView location;
    Context context;

    Profile profile;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tinder_profile_view);
        context = getApplicationContext();
        profile = (Profile) getIntent().getSerializableExtra("profile");

        profile_pic = findViewById(R.id.profile_ImageView);
        ageName = findViewById(R.id.profile_nameAgeTxt);
        location = findViewById(R.id.profile_locationNameTxt);

        try {
            Picasso.with(context).load(profile.getImageUrl()).into(profile_pic);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ageName.setText(profile.getName() + ", " + profile.getAge());
        location.setText(profile.getLocation());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
