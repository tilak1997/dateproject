package com.date;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.mindorks.placeholderview.SwipePlaceHolderView;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.swipe.SwipeCancelState;
import com.mindorks.placeholderview.annotations.swipe.SwipeIn;
import com.mindorks.placeholderview.annotations.swipe.SwipeInState;
import com.mindorks.placeholderview.annotations.swipe.SwipeOut;
import com.mindorks.placeholderview.annotations.swipe.SwipeOutState;
import com.squareup.picasso.Picasso;

@Layout(R.layout.tinder_card_view)
public class TinderCard {

    @View(R.id.profileImageView)
    private ImageView profileImageView;

    @View(R.id.nameAgeTxt)
    private TextView nameAgeTxt;

    @View(R.id.locationNameTxt)
    private TextView locationNameTxt;

    private Profile mProfile;
    private Context mContext;
    private SwipePlaceHolderView mSwipeView;
    Activity activity;

    FrameLayout frameLayout;
    LinearLayout invite_container;

    public TinderCard(Context context, Profile profile, SwipePlaceHolderView swipeView, Activity activity) {
        mContext = context;
        mProfile = profile;
        mSwipeView = swipeView;
        this.activity = activity;
    }

    public void containers(FrameLayout frameLayout,LinearLayout invite_container){
        this.frameLayout = frameLayout;
        this.invite_container = invite_container;
    }

    @Resolve
    private void onResolved(){
        try {
            Picasso.with(mContext).load(mProfile.getImageUrl()).into(profileImageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        Glide.with(mContext).load(mProfile.getImageUrl()).into(profileImageView);
        nameAgeTxt.setText(mProfile.getName() + ", " + mProfile.getAge());
        locationNameTxt.setText(mProfile.getLocation());
    }

    @SwipeCancelState
    private void onSwipeCancelState(){
//        Log.d("EVENT", "onSwipeCancelState");
//        Toast.makeText(mContext, "Cancel state", Toast.LENGTH_SHORT).show();
    }


    @SwipeIn
    private void onSwipeIn(){
        Log.d("EVENT", "onSwipedIn");
//        Toast.makeText(mContext, "swipe in", Toast.LENGTH_SHORT).show();
        new DatabaseHelper(mContext).insert("liked",mProfile);

        int id = mSwipeView.getChildCount();
        if(id == 1){
            listfinished();
        }
    }

    public void listfinished(){
        //this method restarts home activity with
        // certain time lag and start with new set of datas

//            Intent intent = new Intent(mContext,MainActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            mContext.startActivity(intent);

        //this method is instant and continues with previous data set
        invite_container.postDelayed(new Runnable() {
            @Override
            public void run() {
                frameLayout.setVisibility(android.view.View.GONE);
                invite_container.setVisibility(android.view.View.VISIBLE);
            }
        },800);


//        ScaleAnimation animation = new ScaleAnimation(1.0f, 0f, 1.0f, 0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
//        animation.setDuration(1000);
//        frameLayout.setAnimation(animation);

        Animation aniFade = AnimationUtils.loadAnimation(mContext,R.anim.fade_out);
        frameLayout.setAnimation(aniFade);
    }

    @SwipeInState
    private void onSwipeInState(){
//        Log.d("EVENT", "onSwipeInState");
//        Toast.makeText(mContext, "swipe in state", Toast.LENGTH_SHORT).show();
    }

    @SwipeOut
    private void onSwipedOut(){
        Log.d("EVENT", "onSwipedOut");
//        mSwipeView.addView(this);
//        Toast.makeText(mContext, "swipe out", Toast.LENGTH_SHORT).show();
        new DatabaseHelper(mContext).insert("rejected",mProfile);

        int id = mSwipeView.getChildCount();
        if(id == 1){
            listfinished();
        }

    }

    @SwipeOutState
    private void onSwipeOutState(){
//        Log.d("EVENT", "onSwipeOutState");
//        Toast.makeText(mContext, "swipe out state", Toast.LENGTH_SHORT).show();
    }

    @Click(R.id.profileImageView)
    private void onClick(){
        Toast.makeText(mContext, ""+mProfile.getName(), Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(mContext,TinderProfile.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("profile",mProfile);
        mContext.startActivity(intent);

    }

}
