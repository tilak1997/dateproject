package com.date;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Liked_Profiles_Adapter extends ArrayAdapter<Profile> {
    Context context;

    public Liked_Profiles_Adapter(Context context, ArrayList<Profile> list) {
        super(context, 0,list);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView,ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.user_liked_profiles_layout,null);
        final Profile info = getItem(position);

        TextView name =view.findViewById(R.id.liked_profile_name);
        String nameText = "Name  : "+info.getName();
        name.setText(nameText);

        TextView age =view.findViewById(R.id.liked_profile_age);
        String ageText = "Age : "+info.getAge()+" years";
        age.setText(ageText);

        TextView location =view.findViewById(R.id.liked_profile_location);
        location.setText(info.getLocation());

        ImageView image = view.findViewById(R.id.liked_profile_image);
        try {
            Picasso.with(context).load(info.getImageUrl()).into(image);
        } catch (Exception e) {
            e.printStackTrace();
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, ""+info.getName(), Toast.LENGTH_SHORT).show();
            }
        });

        return view;

    }
}
